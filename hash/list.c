/******************************************************************************
 * File:    list.c
 * Author:  Ron Hasson
 * Purpose: This file provides the implementation for a linked list of a
 *          number appearances of a number in a file.
 ******************************************************************************/

#include <string.h>
#include <stdio.h>
#include "list.h"

/******************************************************************************
 *** TYPEDEFS
 ******************************************************************************/

struct NODE
{
    char *filename;
    int amount;
    NODE *next;
};

/******************************************************************************
 *** EXTERNAL FUNCTIONS IMPLEMENTATION
 ******************************************************************************/

/******************************************************************************
 * Func:    LIST_Free
 * Purpose: frees the given linked list (the node and subsequent nodes) from
 *          memory, it will free each node in the list when given the 'head'
 *          of the list.
 * Params:  list - pointer to a node or a list to free
 * Returns: None
 ******************************************************************************/
void LIST_Free(HLIST list)
{
    NODE *current = list;
    NODE *next;

    while (NULL != current)
    {
        next = current->next;
        free(current);
        current = next;
    }
}

/******************************************************************************
 * Func:    LIST_AddNode
 * Purpose: adds a node at the beginning of the given list.
 * Params:  hlist - pointer to a list that will get added a new node.
 *          filename- the file name that contains the appearances of the number.
 *          amount- the amount of appearances of the number in the file.
 * Returns: on success, a new pointer to the list with the new node as the head
 *          of the list. on failure, NULL.
 ******************************************************************************/
HLIST LIST_AddNode(HLIST hlist, char *filename, int amount)
{
    NODE *newNode = (NODE *)malloc(sizeof(NODE));
    if (NULL == newNode)
    {
        return NULL;
    }

    newNode->filename = filename;
    newNode->amount = amount;
    newNode->next = hlist;

    return newNode;
}

/******************************************************************************
 * Func:    LIST_ForEach
 * Purpose: iterates over the list and calls the given callback function.
 * Params:  hlist - pointer to a list.
 *          pnfCallback - the callback function that will be called on each
 *          node of the list.
 * Returns: None.
 ******************************************************************************/
void LIST_ForEach(HLIST hlist, LIST_CALLBACK_FNC pnfCallback)
{
    NODE *current = hlist;

    while (NULL != current)
    {
        pnfCallback(current->filename, current->amount);
        current = current->next;
    }
}