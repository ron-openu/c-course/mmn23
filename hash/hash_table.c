/******************************************************************************
 * File:    hash_table.c
 * Author:  Ron Hasson
 * Purpose: This file provides the implementation for a hash table, an array of
 *          linked lists, that contains the amount of appearances of a number
 *          in a file.
 ******************************************************************************/

#include <stdio.h>
#include "global.h"
#include "list.h"
#include "hash_table.h"

/******************************************************************************
 *** GLOBALS
 ******************************************************************************/

/*Global array of linked lists representing a hash table.*/
static HLIST g_hlist[HASH_TABLE_SIZE] = {0};

/*Global variable to keep track of the current index being processed in the hash
  table.*/
static int g_current_index = 0;

/*Global function pointer to the callback function that will be called for each
  item in the hash table.*/
static HASH_TABLE_CALLBACK_FNC g_callback_function = NULL;

/******************************************************************************
 *** INTERNAL FUNCTIONS IMPLEMENTATION
 ******************************************************************************/

/******************************************************************************
 * Func:    list_CallbackFunction
 * Purpose: Callback function for the LIST_ForEach function.
 * Params:  filename - The name of the file where the number appears.
 *          amount - The number of times the number appears in the file.
 * Returns: None
 ******************************************************************************/
static void list_CallbackFunction(char *filename, int amount)
{
    g_callback_function(g_current_index, filename, amount);
}

/******************************************************************************
 *** EXTERNAL FUNCTIONS IMPLEMENTATION
 ******************************************************************************/

/******************************************************************************
 * Func:    HASH_TABLE_Free
 * Purpose: frees the 29 lists inside the hash table
 * Params:  None
 * Returns: None
 ******************************************************************************/
void HASH_TABLE_Free()
{
    int i = 0;
    for (i = 0; i < HASH_TABLE_SIZE; i++)
    {
        LIST_Free(g_hlist[i]);
        g_hlist[i] = NULL;
    }
}

/******************************************************************************
 * Func:    HASH_TABLE_AddFileAppearances
 * Purpose: adds a record of a number appearances in a file to the corresponding
 *          list in the hash table. (0 goes to the first place in the hash
 *          table, 28 to the last)
 * Params:  number - the number that appeared in the file.
 *          filename- the file name that contains the appearances of the number.
 *          amount- the amount of appearances of the number in the file.
 * Returns: FALSE on success, TRUE on failure.
 ******************************************************************************/
BOOL HASH_TABLE_AddFileAppearances(short number, char *filename, int amount)
{
    HLIST tempList = NULL;
    if (number < 0 || number > MAX_NUMBER)
    {
        return TRUE;
    }

    tempList = LIST_AddNode(g_hlist[number], filename, amount);
    if (NULL == tempList)
    {
        return TRUE;
    }
    g_hlist[number] = tempList;
    return FALSE;
}

/******************************************************************************
 * Func:    HASH_TABLE_ForEach
 * Purpose: iterates over the hash table and calls the given callback function.
 * Params:  pnfCallback - the callback function that will be called on each
 *          item of the hash table.
 * Returns: None.
 ******************************************************************************/
void HASH_TABLE_ForEach(HASH_TABLE_CALLBACK_FNC pnfCallback)
{
    g_callback_function = pnfCallback;
    for (g_current_index = 0; g_current_index < HASH_TABLE_SIZE; g_current_index++)
    {
        HLIST list = g_hlist[g_current_index];
        LIST_ForEach(list, list_CallbackFunction);
    }
}