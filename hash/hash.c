/******************************************************************************
 * File:    main.c
 * Author:  Ron Hasson
 * Purpose: This file reads files from the command line arguments, uses the
 *          hash_table and list modules to store the number appearances, and
 *          prints them in a specified format.
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hash_table.h"
#include "global.h"

#define MIN_ARGS 2

/******************************************************************************
 *** FUNCTION PROTOTYPES
 ******************************************************************************/
static void hash_table_print_callback(short number, char *filename, int amount);
static BOOL process_file(char *filename);

/******************************************************************************
 * Func:    hash_table_print_callback
 * Purpose: callback function to handle printing of the numbers and files.
 *          It keeps track of the last printed number to avoid repetition.
 * Params:  number - the number that appeared in the file.
 *          filename - the file name where the number appeared.
 *          amount - the number of appearances of the number in the file.
 * Returns: None
 * Remark:
 ******************************************************************************/
static void hash_table_print_callback(short number, char *filename, int amount)
{
   /*A static variable to keep track of the last number printed.*/
   static int lastNumber = -1;
   if (lastNumber != number)
   {
      printf("\n%d appears in file %s - %d time%s", number, filename, amount, amount > 1 ? "s" : "");
      lastNumber = number;
   }
   else
   {
      printf(", file %s - %d time%s", filename, amount, amount > 1 ? "s" : "");
   }
}

/******************************************************************************
 * Func:    process_file
 * Purpose: Processes a file to extract number appearances and adds them to the
 *          hash table.
 * Params:  filename - the name of the file to process.
 * Returns: FALSE on success, TRUE on failure.
 ******************************************************************************/
static BOOL process_file(char *filename)
{
   FILE *file;
   int i = 0;
   /*used for the found number by scanf*/
   unsigned short number;
   /*temporary numbers array for each file in order to push only once to the hash
   table without the need to update each time */
   unsigned short numbersArr[HASH_TABLE_SIZE] = {0};

   file = fopen(filename, "r");
   if (NULL == file)
   {
      printf("Error opening file");
      return TRUE;
   }

   while (0 < fscanf(file, "%hu", &number))
   {
      numbersArr[number]++;
   }

   /*pushing everything in the numbersArr to the hash table */
   for (i = 0; i < HASH_TABLE_SIZE; i++)
   {
      if (numbersArr[i] > 0)
      {
         if (HASH_TABLE_AddFileAppearances(i, filename, numbersArr[i]))
         {
            printf("error while trying to insert to hash table");
            fclose(file);
            return TRUE;
         }
      }
   }

   fclose(file);
   return FALSE;
}

/******************************************************************************
 * Func:    main
 * Purpose: Main function to read files from the command line, process them,
 *          and print the number appearances.
 * Params:  argc - the number of command line arguments.
 *          argv - the array of command line arguments.
 * Returns: EXIT_SUCCESS on success, EXIT_FAILURE on failure.
 ******************************************************************************/
int main(int argc, char *argv[])
{
   int i = 1;
   /*if there is no arguments(files) provided*/
   if (argc < MIN_ARGS)
   {
      printf("Usage: %s <file1> <file2> ... <fileN>\n", argv[0]);
      return EXIT_FAILURE;
   }

   /*proccess all the files from the args*/
   for (i = argc - 1; i > 0; i--)
   {
      if (process_file(argv[i]))
      {
         HASH_TABLE_Free();
         return EXIT_FAILURE;
      }
   }

   /*print the number appearances*/
   HASH_TABLE_ForEach(hash_table_print_callback);
   printf("\n");

   HASH_TABLE_Free();

   return EXIT_SUCCESS;
}
