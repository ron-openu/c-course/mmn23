/******************************************************************************
 * File:    hash_table.h
 * Author:  Ron Hasson
 * Purpose: This header file defines the interface for a hash table, an array of
 *          linked lists, that contains the amount of a appearances of a number
 *          in a file. each index of the array corresponds to a number, each node
 *          in a list corresponds to a file and the number of appearances of
 *          the number.
 ******************************************************************************/
#ifndef HASH_TABLE_H
#define HASH_TABLE_H

/******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <stdlib.h>
#include "list.h"
#include "global.h"

/******************************************************************************
 *** MACROS
 ******************************************************************************/

#define MAX_NUMBER 28
#define HASH_TABLE_SIZE (MAX_NUMBER + 1)

/******************************************************************************
 *** TYPEDEFS
 ******************************************************************************/

/* Function pointer type for hash table forEach callback. */
typedef void (*HASH_TABLE_CALLBACK_FNC)(short number, char *filename, int amount);

/******************************************************************************
 *** EXTERNAL FUNCTIONS
 ******************************************************************************/

/******************************************************************************
 * Func:    HASH_TABLE_Free
 * Purpose: frees the 29 lists inside the hash table
 * Params:  None
 * Returns: None
 ******************************************************************************/
void HASH_TABLE_Free();

/******************************************************************************
 * Func:    HASH_TABLE_AddFileAppearances
 * Purpose: adds a record of a number appearances in a file to the corresponding
 *          list in the hash table. (0 goes to the first place in the hash
 *          table, 28 to the last)
 * Params:  number - the number that appeared in the file.
 *          filename- the file name that contains the appearances of the number.
 *          amount- the amount of appearances of the number in the file.
 * Returns: FALSE on success, TRUE on failure.
 ******************************************************************************/
BOOL HASH_TABLE_AddFileAppearances(short number, char *filename, int amount);

/******************************************************************************
 * Func:    HASH_TABLE_ForEach
 * Purpose: iterates over the hash table and calls the given callback function.
 * Params:  pnfCallback - the callback function that will be called on each
 *          item of the hash table.
 * Returns: None.
 ******************************************************************************/
void HASH_TABLE_ForEach(HASH_TABLE_CALLBACK_FNC pnfCallback);

#endif