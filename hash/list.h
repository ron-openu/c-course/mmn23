/******************************************************************************
 * File:    list.h
 * Author:  Ron Hasson
 * Purpose: This header file defines the interface for a linked list of a
 *          number appearances of a number in a file. each node represents a
 *          file and its number of appearances of the number between 0 to 28.
 *          This file provides functions to operate on linked lists.
 ******************************************************************************/
#ifndef LIST_H
#define LIST_H

/******************************************************************************
 *** INCLUDES
 ******************************************************************************/
#include <stdlib.h>
#include "global.h"

/******************************************************************************
 *** TYPEDEFS
 ******************************************************************************/

/*A structure representing a node in the list that contains the file name and
the number of appearances of a number in that file.*/
typedef struct NODE NODE;

/*A pointer to a NODE structure.*/
typedef NODE *HLIST;

/* Function pointer type for list forEach callback. */
typedef void (*LIST_CALLBACK_FNC)(char *filename, int amount);

/******************************************************************************
 *** EXTERNAL FUNCTIONS
 ******************************************************************************/

/******************************************************************************
 * Func:    LIST_Free
 * Purpose: frees the given linked list (the node and subsequent nodes) from
 *          memory, it will free each node in the list when given the 'head'
 *          of the list.
 * Params:  list - pointer to a node or a list to free
 * Returns: None
 ******************************************************************************/
void LIST_Free(HLIST list);

/******************************************************************************
 * Func:    LIST_AddNode
 * Purpose: adds a node at the beginning of the given list.
 * Params:  hlist - pointer to a list that will get added a new node.
 *          filename- the file name that contains the appearances of the number.
 *          amount- the amount of a appearances of the number in the file.
 * Returns: on success, a new pointer to the list with the new node as the head
 *          of the list. on failure, NULL.
 ******************************************************************************/
HLIST LIST_AddNode(HLIST hlist, char *filename, int amount);

/******************************************************************************
 * Func:    LIST_ForEach
 * Purpose: iterates over the list and calls the given callback function.
 * Params:  hlist - pointer to a list.
 *          pnfCallback - the callback function that will be called on each
 *          node of the list.
 * Returns: None.
 ******************************************************************************/
void LIST_ForEach(HLIST hlist, LIST_CALLBACK_FNC pnfCallback);

#endif